package structuralPattern;

public class ShopKeeper {  
    private CarShop Audi;  
    private CarShop Acura;  
    private CarShop Volvo; 
    private CarShop Toyota;
      
    public ShopKeeper(){  
    	Audi= new Audi();  
    	Acura=new Acura();  
        Volvo=new Volvo();  
        Toyota =new Toyota();
    }  
    public void AudiSale(){  
    	Audi.model();  
    	Audi.price();  
    }  
        public void AcuraSale(){  
        	Acura.model();  
        	Acura.price();  
    }  
   public void VolvoSale(){  
	   Volvo.model();  
	   Volvo.price();  
        }  
   public void Toyota() {
	   Toyota.model();
	   Toyota.price();
   } 
} 
