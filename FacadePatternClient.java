package structuralPattern;

import java.io.BufferedReader;  
import java.io.IOException;  
import java.io.InputStreamReader;  
  
public class FacadePatternClient {  
    private static int  choice;  
    public static void main(String args[]) throws NumberFormatException, IOException{  
        do{       
            System.out.print("========= Car Shop    ===========\n");  
            System.out.print("            1. Audi.              \n");  
            System.out.print("            2. Acura.              \n");  
            System.out.print("            3. Volvo.               \n");  
            System.out.print("            4. Toyota.               \n");
            System.out.print("            5. Exit.                  \n");  
            System.out.print("Enter your choice: ");  
              
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));  
            choice=Integer.parseInt(br.readLine());  
            ShopKeeper sk=new ShopKeeper();  
              
            switch (choice) {  
            case 1:  
                {   
                  sk.AudiSale();  
                    }  
                break;  
       case 2:  
                {  
                  sk.AcuraSale();        
                    }  
                break;    
       case 3:  
                           {  
                           sk.VolvoSale();     
                           }  
                   break;
       case 4:
       						{
       							sk.Toyota();
       						}
       break;
            default:  
            {    
                System.out.println("Nothing You purchased");  
            }         
                return;  
            }  
              
      }while(choice!=5);  
   }  
}  